var Carrot = Class.create();
Carrot.prototype = {
    initialize: function(getProductsUrl) {
        this.getProductsUrl = getProductsUrl;
        this.initProductsDropdownListener();
    },

    loadProductsDropdown: function() {
        var $storeId = $('store_id').getValue();
        if ($storeId.length) {
            new Ajax.Request(carrot.getProductsUrl, {
                method: 'get',
                loaderArea: true,
                parameters: 'store_id=' + $storeId,
                onComplete: function(transport) {
                    var response = transport.responseText.evalJSON();
                    var products = response.products;
                    $('product_id').update(products).removeAttribute('disabled');
                }
            });
        } else {
            $('product_id').setValue('').setAttribute('disabled', 'disabled');
        }
    },

    initProductsDropdownListener: function() {
        Event.observe('store_id', 'change', this.loadProductsDropdown);
    }
}