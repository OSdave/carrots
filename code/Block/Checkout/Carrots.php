<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Checkout_Carrots extends Mage_Core_Block_Template
{

    public function getCarrotsUrl()
    {
	return $this->getUrl('carrots/checkout/getAvailableCarrots');
    }

    public function getCarrotsToShow()
    {
	$storeId = Mage::app()->getStore()->getId();
	$cartItemsSubtotalWithTaxes = Mage::helper('carrots')->getCartItemsValueIncTax();

	$carrotsForThisCart = Mage::getModel('carrots/carrots')->getCollection()
	    ->addStoreFilter($storeId)
	    ->addThresholdFilter($cartItemsSubtotalWithTaxes)
	    ->addProductData()
	    ->setDataToAll('current_total', $cartItemsSubtotalWithTaxes);

	return $carrotsForThisCart;
    }

}