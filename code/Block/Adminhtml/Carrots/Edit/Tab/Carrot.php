<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots_Edit_Tab_Carrot extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('info', array('legend' => Mage::helper('carrots')->__('Carrot')));

        $fieldset->addField('active', 'select', array(
            'label' => Mage::helper('carrots')->__('Status'),
            'name' => 'active',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('carrots')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('carrots')->__('Disabled'),
                ),
            ),
        ));


        $fieldset->addField('threshold', 'text', array(
            'label' => Mage::helper('carrots')->__('threshold'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'threshold',
            'note' => Mage::helper('carrots')->__("The amount (Tax Incl.) the cart must reach to get this carrot.")
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name' => 'stores[]',
                'label' => Mage::helper('cms')->__('Store View'),
                'title' => Mage::helper('cms')->__('Store View'),
                'required' => true,
                'class' => 'required-entry',
                'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'store_id[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('product_id', 'select', array(
            'label' => Mage::helper('carrots')->__('Product'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'product_id',
            'disabled' => 'disabled',
            'values' => $this->_productsDropdown(),
            'note' => Mage::helper('carrots')->__("Only the products enabled in all selected store views will show up")
        ));

//        $fieldset->addField('label', 'text', array(
//            'label' => Mage::helper('carrots')->__('Label'),
//            'required' => false,
//            'name' => 'label',
//            'note' => Mage::helper('carrots')->__("The name that will appear: leave blank to use original product's name")
//        ));

        if (Mage::getSingleton('adminhtml/session')->getCarrotData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getCarrotData());
        } elseif (Mage::registry('carrot_data')) {
            $form->setValues(Mage::registry('carrot_data')->getData());
        }
        return parent::_prepareForm();
    }

    /**
     * prepare dropdown of products
     */
    protected function _productsDropdown()
    {
        $products = array(array(
            'value' => '',
            'label' => Mage::helper('carrots')->__('Select a store view')
        ));

        return $products;
    }

}