<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots_Edit_Tab_Products extends Mage_Core_Block_Html_Select
{

    protected function _getProducts()
    {
        $storeIdGet = $this->getRequest()->getParam('store_id');
        $storeIds = explode(',', $storeIdGet);
        $products = array(array(
            'value' => '',
            'label' => Mage::helper('carrots')->__('Select a product')
        ));

        $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('name')
                ->addFieldToFilter('status', 1)//enabled
                ->addAttributeToSort('name', 'ASC');
        foreach ($storeIds as $storeId) {
            $collection->addStoreFilter($storeId);
        }

        foreach ($collection->getData() as $product) {
            $products[] = array(
                'value' => $product['entity_id'],
                'label' => $product['name'],
            );
        }

        return $products;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            if (Mage::getSingleton('adminhtml/session')->getCarrotData()) {
                $carrot = Mage::getSingleton('adminhtml/session')->getCarrotData();
            } elseif (Mage::registry('carrot_data')) {
                $carrot = Mage::registry('carrot_data')->getData();
            }
            foreach ($this->_getProducts() as $product) {
                if ($carrot->getProductId() == $product['value']) {
                    $params = array('selected' => 'selected');
                } else {
                    $params = array();
                }
                $this->addOption($product['value'], $product['label'], $params);
            }
        }
        return parent::_toHtml();
    }

}