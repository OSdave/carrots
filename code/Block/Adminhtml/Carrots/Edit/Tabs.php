<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('carrot_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('carrots')->__('Carrot'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('carrot', array(
            'label' => Mage::helper('carrots')->__('Carrot'),
            'title' => Mage::helper('carrots')->__('Carrot'),
            'content' => $this->getLayout()->createBlock('carrots/adminhtml_carrots_edit_tab_carrot')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}