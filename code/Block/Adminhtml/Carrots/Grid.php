<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('carrotsGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('carrots/carrots')->getCollection();
        $collection->setFirstStoreFlag(true);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('threshold', array(
            'header' => Mage::helper('carrots')->__('Threshold'),
            'align' => 'left',
            'index' => 'threshold',
            'type' => 'currency',
            'currency_code' => $this->_getCurrentCurrencyCode()
        ));

        $this->addColumn('product', array(
            'header' => Mage::helper('carrots')->__('Carrot Product'),
            'align' => 'left',
            'index' => 'product_id',
      	   'renderer' => 'carrots/adminhtml_carrots_grid_renderer_product',
            'filter_condition_callback' => array($this, '_filterProduct')
        ));

//        $this->addColumn('price', array(
//            'header' => Mage::helper('carrots')->__('Price'),
//            'align' => 'left',
//            'index' => 'price',
//            'type' => 'currency',
//            'currency_code' => $this->_getCurrentCurrencyCode()
//        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => Mage::helper('cms')->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'sortable' => false,
                'filter_condition_callback' => array($this, '_filterStoreCondition')
            ));
        }

        $this->addColumn('status', array(
            'header' => Mage::helper('carrots')->__('Status'),
            'align' => 'left',
            'width' => '90px',
            'index' => 'active',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled',
            )
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('carrots')->__('Action'),
            'width' => '40',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('carrots')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
//            'index' => 'stores',
            'is_system' => true
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('carrots')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('carrots')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('carrots');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('carrots')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('carrots')->__('Are you sure?')
        ));

        Mage::unregister('_singleton/carrots/active');

        $statuses = Mage::getSingleton('carrots/status')->getOptionArray();

        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('carrots')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('carrots')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * get currency code for the store
     *
     * @return String
     */
    protected function _getCurrentCurrencyCode()
    {
        if (is_null($this->_currentCurrencyCode)) {
            $this->_currentCurrencyCode = (count($this->_storeIds) > 0)
                ? Mage::app()->getStore(array_shift($this->_storeIds))->getBaseCurrencyCode()
                : Mage::app()->getStore()->getBaseCurrencyCode();
        }
        return $this->_currentCurrencyCode;
    }

    protected function _filterProduct($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $products = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter(
                array(
                    array('attribute' => 'name', 'like' => '%' . $value . '%')
                )
        );

        $filteredProducts = array();
        foreach ($products as $product) {
            $filteredProducts[] = $product->getId();
        }

        $collection->addFieldToFilter('product_id', array('in' => $filteredProducts));
    }

}