<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'carrots';
        $this->_controller = 'Adminhtml_Carrots';

        $this->_updateButton('save', 'label', Mage::helper('carrots')->__('Save Carrot'));
        $this->_updateButton('delete', 'label', Mage::helper('carrots')->__('Delete Carrot'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }

            var carrot = new Carrot('" . Mage::helper('carrots')->getProductsDropdownUrl() . "');
            carrot.loadProductsDropdown();
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('carrot_data') && Mage::registry('carrot_data')->getId()) {
            return Mage::helper('carrots')->__("Carrot '%s'", $this->htmlEscape(Mage::registry('carrot_data')->getLabel()));
        } else {
            return Mage::helper('carrots')->__('New Carrot');
        }
    }

}