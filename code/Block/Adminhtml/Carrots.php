<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Block_Adminhtml_Carrots extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_carrots';
        $this->_blockGroup = 'carrots';
        $this->_headerText = Mage::helper('carrots')->__('Carrots');
        $this->_addButtonLabel = Mage::helper('carrots')->__('Add a Carrot');
        parent::__construct();
    }

}