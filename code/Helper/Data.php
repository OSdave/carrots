<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Osdave_Carrots_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getProductsDropdownUrl()
    {
        return Mage::helper("adminhtml")->getUrl("carrots/adminhtml_index/getProducts/");
    }

    public function getCartItemsValueIncTax($quote = null)
    {
	if (is_null($quote)) {
	    $quote = Mage::getSingleton('checkout/cart')->getQuote();
	}
	
	$cartItems = $quote->getAllItems();
	$cartItemsSubtotalWithTaxes = 0;
	foreach ($cartItems as $item) {
	    $cartItemsSubtotalWithTaxes += $item->getRowTotalInclTax();
	}

	return $cartItemsSubtotalWithTaxes;
    }
}