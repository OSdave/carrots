<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_CheckoutController extends Mage_Core_Controller_Front_Action
{
    public function getAvailableCarrotsAction()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_carrots_available');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('carrots' => $output)));
    }
}