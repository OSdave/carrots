<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
                ->_setActiveMenu('sales/carrots')
                ->_addBreadcrumb(Mage::helper('carrots')->__('Manage Carrots'), Mage::helper('carrots')->__('Manage Carrot'));

        return $this;
    }

    public function indexAction()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_initAction();
        $this->_title($this->__('Carrots'))->_title($this->__('Manage Carrots'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('carrots/adminhtml_carrots_grid')->toHtml());
    }

    public function editAction()
    {
        $this->_title($this->__('Carrots'))->_title($this->__('Manage Carrot'));
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('carrots/carrots')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::getSingleton('adminhtml/session')->setCarrotData($model);

            $this->loadLayout();
            $this->_setActiveMenu('sales/carrots');

            $this->_addBreadcrumb(Mage::helper('carrots')->__('Manage Carrot'), Mage::helper('carrots')->__('Manage Carrot'));
            $this->_addBreadcrumb(Mage::helper('carrots')->__('Carrot'), Mage::helper('carrots')->__('Carrot'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('carrots/adminhtml_carrots_edit'))
                    ->_addLeft($this->getLayout()->createBlock('carrots/adminhtml_carrots_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('carrots')->__('Carrot does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function getProductsAction()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('carrot_products');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('products' => $output)));
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('carrots/carrots');
            $model->setData($data)
                    ->setId($this->getRequest()->getParam('id'));

            try {
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('carrots')->__('Carrot was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('carrots')->__('Unable to find Carrot to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('carrots/carrots');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('carrots')->__('Carrot was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $carrotsIds = $this->getRequest()->getParam('carrots');
        if (!is_array($carrotsIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('carrots')->__('Please select Carrot(s)'));
        } else {
            try {
                foreach ($carrotsIds as $carrotsId) {
                    $carrots = Mage::getModel('carrots/carrots')->load($carrotsId);
                    $carrots->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('carrots')->__(
                                'Total of %d record(s) were successfully deleted', count($carrotsIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $carrotsIds = $this->getRequest()->getParam('carrots');
        if (!is_array($carrotsIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Carrot(s)'));
        } else {
            try {
                foreach ($carrotsIds as $carrotId) {
                    $carrots = Mage::getSingleton('carrots/messages')
                            ->load($carrotId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($carrotsIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'carrots_messages.csv';
        $content = $this->getLayout()->createBlock('carrots/adminhtml_carrots_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'carrots_messages.xml';
        $content = $this->getLayout()->createBlock('carrots/adminhtml_carrots_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

}