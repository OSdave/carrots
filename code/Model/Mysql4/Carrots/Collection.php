<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Model_Mysql4_Carrots_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    public function _construct()
    {
        $this->_init('carrots/carrots');
        $this->_map['fields']['id'] = 'main_table.id';
        $this->_map['fields']['store']   = 'store_table.store_id';
    }

    /**
     * Set first store flag
     *
     * @param bool $flag
     * @return Osdave_Carrots_Model_Mysql4_Carrots_Collection
     */
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    /**
     * Perform operations after collection load
     *
     * @return Osdave_Carrots_Model_Mysql4_Carrots_Collection
     */
    protected function _afterLoad()
    {
        if ($this->_previewFlag) {
            $items = $this->getColumnValues('id');
            $connection = $this->getConnection();
            if (count($items)) {
                $select = $connection->select()
                        ->from(array('cps'=>$this->getTable('carrots/carrots_store')))
                        ->where('cps.id IN (?)', $items);

                if ($result = $connection->fetchPairs($select)) {
                    foreach ($this as $item) {
                        if (!isset($result[$item->getData('id')])) {
                            continue;
                        }
                        if ($result[$item->getData('id')] == 0) {
                            $stores = Mage::app()->getStores(false, true);
                            $storeId = current($stores)->getId();
                            $storeCode = key($stores);
                        } else {
                            $storeId = $result[$item->getData('id')];
                            $storeCode = Mage::app()->getStore($storeId)->getCode();
                        }
                        $item->setData('_first_store_id', $storeId);
                        $item->setData('store_code', $storeCode);
                    }
                }
            }
        }

        return parent::_afterLoad();
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Osdave_Carrots_Model_Mysql4_Carrots_Collection
     */
    public function addStoreFilter($storeId, $withAdmin = true)
    {
        $this->getSelect()->join(
            array('store_table' => $this->getTable('carrots/carrots_store')),
            'main_table.id = store_table.id',
            array()
        )
        ->where('store_table.store_id in (?)', ($withAdmin ? array(0, $storeId) : $storeId))
        ->group('main_table.id');

        return $this;
    }

    public function addThresholdFilter($cartTotal)
    {
        $this->addFieldToFilter('threshold', array(
            'gt' => $cartTotal
        ));

        return $this;
    }

    public function addProductData()
    {
	$carrotsIds = $this->getColumnValues('product_id');
	$productCollection = Mage::getModel('catalog/product')->getCollection()
	    ->addAttributeToFilter('entity_id', array(
		'in' => $carrotsIds
	    ))
	    ->addAttributeToSelect(array('name', 'thumbnail'));
	$deb = $productCollection->getSelect()->assemble();
	$productCollection->getSelect()->join(
	    array('carrot' => $this->getTable('carrots/carrots')),
	    'e.entity_id = carrot.product_id',
	    array('carrot.threshold')
	);
	$deb = $productCollection->getSelect()->assemble();

	return $productCollection;
    }

    public function getCarrotForQuote($cartTotal)
    {
	$this->addFieldToFilter('threshold', array(
            'lt' => $cartTotal
        ))
	    ->addOrder('threshold', 'DESC');
	$deb = $this->getSelect()->assemble();

        return $this;
    }

}