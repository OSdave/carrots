<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Model_Carrots extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
	parent::_construct();
	$this->_init('carrots/carrots');
    }

}