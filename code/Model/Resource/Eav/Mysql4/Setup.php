<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Model_Resource_Eav_Mysql4_Setup extends Mage_Sales_Model_Mysql4_Setup
{
    //nothing here, just need to extend the parent in order to create quote attribute
}