<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED = 0;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('carrots')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('carrots')->__('Disabled')
        );
    }
}