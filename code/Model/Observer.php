<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Osdave_Carrots_Model_Observer
{

    const XML_PATH_CARROTS_CONFIG_ACTIVE = 'carrots/config/active';

    public function setCarrotInCartTemplate($observer)
    {
	$block = $observer->getEvent()->getBlock();
	$class = get_class($block);
	if ($class == 'Mage_Checkout_Block_Cart_Item_Renderer') {
	    $template = $block->getTemplate();
	    if ($block->getItem()->hasCarrot()) {
		if (stristr($template, 'sidebar')) {//hide carrots in sidebar "recently added items" block
		    $block->setTemplate('osdave/carrots/checkout/cart/sidebar/carrot.phtml');
		} elseif (stristr($template, 'cart')) {
		    $block->setTemplate('osdave/carrots/checkout/cart/item/carrot.phtml');
		} elseif (stristr($template, 'review')) {
		    $block->setTemplate('osdave/carrots/checkout/review/carrot.phtml');
		}
	    } elseif (stristr($template, 'sidebar')) {
		$block->setTemplate('checkout/cart/sidebar/default.phtml');
	    }
	}
    }

    public function setCarrotInQuote($observer)
    {
	if (Mage::getStoreConfig(self::XML_PATH_CARROTS_CONFIG_ACTIVE, Mage::app()->getStore()->getId())) {
	    $quote = $observer->getEvent()->getQuote();
	    $subtotalWithTaxes = Mage::helper('carrots')->getCartItemsValueIncTax($quote);

	    $this->_deleteCarrot($quote);

	    if ($subtotalWithTaxes > 0) {
		$carrotForThisQuote = Mage::getModel('carrots/carrots')->getCollection()
		    ->addStoreFilter($quote->getStoreId())
		    ->getCarrotForQuote($subtotalWithTaxes);
		if ($carrotForThisQuote->getSize()) {
		    $this->_addCarrot($quote, $carrotForThisQuote->getFirstItem()->getProductId());
		}
	    }
	}

	return $this;
    }

    private function _addCarrot($quote, $productId)
    {
	$carrot = Mage::getModel('catalog/product')->load($productId);
	$quote->addProduct($carrot)->setCarrot(1);
    }

    private function _deleteCarrot($quote)
    {
	$quoteItems = $quote->getAllItems();
	foreach ($quoteItems as $item) {
	    if ($item->hasCarrot()) {
		$quote->removeItem($item->getItemId());
	    }
	}
    }

}