<?php

/**
 * @category   Osdave
 * @package    Osdave_Carrots
 * @author     Osdave <david.parloir@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('carrots/carrots'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true
        ), 'Carrot ID')
    ->addColumn('active', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, array(
        'nullable'  => false,
        'default'   => '1'
        ), 'Is Carrot Active')
    ->addColumn('threshold', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '0.0000'
        ), 'Carrot Threshold')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => false,
        'unsigned' => true
        ), 'Product ID')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => true
        ), 'Carrot price')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_VARCHAR, '200', array(
        'nullable'  => true
        ), 'Label')
    ->addIndex($installer->getIdxName('carrots/carrots', array('threshold')),
        array('threshold'))
    ->setComment('Carrots Table');
$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('carrots/carrots_store'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array(
        'nullable'  => false,
        'primary'   => true,
        'unsigned' => true
        ), 'Carrot ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('carrots/carrots_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('carrots/carrots_store', 'id', 'carrots/carrots', 'id'),
        'id', $installer->getTable('carrots/carrots'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('carrots/carrots_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Carrots To Store Linkage Table');
$installer->getConnection()->createTable($table);

//carrot attribute for quote
$options = array(
    'type'     => Varien_Db_Ddl_Table::TYPE_TINYINT,
    'visible'  => false,
    'required' => false
);

$installer->addAttribute('quote_item', 'carrot', $options);
Mage::getResourceModel('catalog/setup', 'catalog_setup')->addAttribute(
    Mage_Catalog_Model_Product::ENTITY, 'carrot',
    array(
        'group'         => 'General',
        'backend'       => 'catalog/product_attribute_backend_boolean',
        'frontend'      => '',
        'label'         => 'Is Carrot',
        'input'         => 'select',
        'class'         => '',
        'source'        => 'eav/entity_attribute_source_boolean',
        'global'        => true,
        'visible'       => false,
        'required'      => false,
        'user_defined'  => false,
        'default'       => '',
        'apply_to'      => '',
        'input_renderer'   => 'giftmessage/adminhtml_product_helper_form_config',
        'is_configurable'  => 0,
        'visible_on_front' => false
    )
);

$installer->endSetup();
