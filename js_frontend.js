var Carrot = Class.create();
Carrot.prototype = {
    initialize: function(getCarrotsUrl) {
        this.getCarrotsUrl = getCarrotsUrl;//url for ajax request
        this.displayCarrotsInCheckoutCart();
    },

    displayCarrotsInCheckoutCart: function() {
        new Ajax.Request(this.getCarrotsUrl, {
            method: 'get',
            loaderArea: true,
            onComplete: function(transport) {
                var response = transport.responseText.evalJSON();
                $$('.cart-collaterals')[0].insert({
                    'top' : response.carrots
                });
                decorateList('available_carrots_list', 'none-recursive')
            }
        });
    }
}